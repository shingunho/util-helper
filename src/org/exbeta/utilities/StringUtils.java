package org.exbeta.utilities;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StringUtils {

	private StringUtils() {
	}

	/**
	 * Object's null checker
	 * 
	 * @param Object
	 *            s
	 * @return answer about the 'Is this null?' question
	 */
	public static <T> boolean isNull(T s) {
		return (s == null || s.toString().trim().isEmpty()) ? true : false;
	}
	
	public static String[] split(String str, String delimiter){
		String de = delimiter;
		if(de.equals(".")){
			de = "\\.";
		}
		return str.split(de);
	}

	/**
	 * Default Parser
	 * 
	 * @see default ";" on element set separator, and "=" is key-value pair
	 *      define
	 * @param String
	 *            str
	 * @return parsed Map<K, V> object
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> parseToMapLikeWebSeparator(String str) {
		Map<K, V> r = new HashMap<K, V>();
		String[] elem = str.split(";");
		for (String e : elem) {
			String[] pair = e.split("=");
			System.out.println(e.split("=").length);
			if (pair.length > 2) {
				throw new IllegalArgumentException();
			}
			if (pair.length == 1) {
				System.out
						.println("[ERROR:"
								+ new Date().getTime()
								+ "] Key-value pair has broken. Can't read key or value at - "
								+ new IllegalArgumentException().getCause());
				break;
			}
			try {
				r.put((K) pair[0], (V) pair[1]);
			} catch (ClassCastException ex) {
				ex.printStackTrace();
				System.out.println("[ERROR:" + new Date().getTime()
						+ "] Can't cast arguments at - " + ex.getCause());
			}
		}

		return r;
	}

	/**
	 * String parse to map using custom separator
	 * 
	 * @param String
	 *            str
	 * @param String
	 *            elementSeparator
	 * @param String
	 *            keyValueSeparator
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> parseToMap(String str,
			String elementSeparator, String keyValueSeparator) {
		Map<K, V> r = new HashMap<K, V>();
		String[] elem = str.split(elementSeparator);
		for (String e : elem) {
			String[] pair = e.split(keyValueSeparator);
			if (pair.length > 2) {
				throw new IllegalArgumentException();
			}
			if (pair.length == 1) {
				System.out
						.println("[ERROR:"
								+ new Date().getTime()
								+ "] Key-value pair has broken. Can't read key or value at - "
								+ new IllegalArgumentException().getCause());
				break;
			}
			try {
				r.put((K) pair[0], (V) pair[1]);
			} catch (ClassCastException ex) {
				ex.printStackTrace();
				System.out.println("[ERROR:" + new Date().getTime()
						+ "] Can't cast arguments at - " + ex.getCause());
			}
		}

		return r;
	}

	@SuppressWarnings("unchecked")
	public static <T> T parseToObject(String str, String elementSeparator,
			String keyValueSeparator, Class<T> cl) throws InstantiationException, IllegalAccessException{
		Object result = cl.newInstance();
		
		Map<String, String> parsedMap = StringUtils.parseToMap(str, elementSeparator,
				keyValueSeparator);
		
		Field[] fields = result.getClass().getDeclaredFields();
		Object[] mapKeys = MapUtils.getKeys(parsedMap);
		for (Field f : fields) {
			f.setAccessible(true);
			try {
				for(int i=0;i<mapKeys.length;i++){
					String v = mapKeys[i].toString();
					//System.out.println("KEY : " + v + ", Field name : " + f.getName());
					if(v.equals(f.getName())){
						System.out.println("Parse To Object : " + f.getName() + ", set : " + parsedMap.get(f.getName()));
						Object value = null;
						if(f.getType().isAssignableFrom(int.class) || f.getType().isAssignableFrom(Integer.class)){
							value = Integer.parseInt(parsedMap.get(f.getName()));
						}else if(f.getType().isAssignableFrom(String.class)){
							value = (String) parsedMap.get(f.getName());
						}else if(f.getType().isAssignableFrom(float.class) || f.getType().isAssignableFrom(Float.class)){
							value = Float.parseFloat(parsedMap.get(f.getName()));
						}else if(f.getType().isAssignableFrom(double.class) || f.getType().isAssignableFrom(Double.class)){
							value = Double.parseDouble(parsedMap.get(f.getName()));
						}else if(f.getType().isAssignableFrom(long.class) || f.getType().isAssignableFrom(Long.class)){
							value = Long.parseLong(parsedMap.get(f.getName()));
						}
						f.set(result, value);
						break;
					}
				}
			} catch (IllegalArgumentException e) {
				System.out.println("[EXCEPTION OCCURED]: " + Calendar.getInstance().getTime() + " "+ e.getCause() + "\n" + e.getStackTrace());
				e.printStackTrace();
			}
			f.setAccessible(false);
		}
		return (T) result;
	}

	/**
	 * String reverse
	 * 
	 * @param String
	 *            s
	 * @return reversed string
	 */
	public static String reverse(String s) {
		String result = "";
		int i = s.length();
		if (StringUtils.isNull(s)) {
			System.out.println("[ERROR:" + new Date().getTime()
					+ "] argument is NULL at - "
					+ new NullPointerException().getCause());
			return null;
		}
		while (i > 0) {
			result += s.charAt(i - 1);
			i--;
		}
		return result;
	}

}

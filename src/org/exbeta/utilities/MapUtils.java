package org.exbeta.utilities;

import java.util.Iterator;
import java.util.Map;

public class MapUtils {
	
	private MapUtils(){}

	@SuppressWarnings("unchecked")
	public static <K> K[] getKeys(Map<K, ?> m){
		Iterator<?> keySets = m.keySet().iterator();
		K[] keys = (K[]) new Object[m.keySet().size()];
		int i=0;
		while(keySets.hasNext()){
			keys[i] = (K) keySets.next();
			i++;
		}
		return keys;
	}
	
	@SuppressWarnings("unchecked")
	public static <K, V> V[] getValues(Map<K, V> m){
		K[] keys = MapUtils.getKeys(m);
		V[] values = (V[]) new Object[keys.length];
		for(int i=0;i<keys.length;i++){
			values[i] = m.get(keys[i]);
		}
		return values;
	}
}

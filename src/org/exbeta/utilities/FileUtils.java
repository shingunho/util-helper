package org.exbeta.utilities;

import java.io.File;

public class FileUtils {

	private String fileAbsolutePath;

	public FileUtils(String fileAbsolutePath) {
		this.fileAbsolutePath = fileAbsolutePath;
	}

	public void erase() {
		File f = new File(fileAbsolutePath);
		if (f.isFile()) {
			f.delete();
		}
	}

	public void moveTo(String destinationPath) {
		File f = new File(fileAbsolutePath);

		if (f.renameTo(new File(destinationPath + f.getName()))) {
			System.out.println("INFO : file move successfully");
		} else {
			System.out.println("INFO : file move failed");
		}
	}
}

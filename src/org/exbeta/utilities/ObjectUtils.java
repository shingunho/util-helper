package org.exbeta.utilities;

import java.lang.reflect.Field;

public class ObjectUtils<T> {

	private ObjectUtils() {
	}

	private static <T> Field[] getFields(T obj) {
		return obj.getClass().getDeclaredFields();
	}

	/**
	 * 
	 * @param Object
	 *            obj
	 * @return private Field's 'type + name + value' in some object
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static String toString(Object obj) throws IllegalArgumentException,
			IllegalAccessException {
		StringBuffer sb = new StringBuffer();
		sb.append(obj.getClass().getName() + "'s infomations :\n");

		for (Field f : ObjectUtils.getFields(obj)) {
			f.setAccessible(true);
			sb.append("    [ FIELD ] : " + f.get(obj).getClass() + " "
					+ f.getName() + " : " + f.get(obj).toString() + "\n");
			f.setAccessible(false);
		}
		return sb.toString();
	}

	/**
	 * Object Setter using Object's Field variable name
	 * @param String category
	 * @param String value
	 * @param Destination Objectn obj
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static <T> void set(String variableName, String value, T obj)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = obj.getClass().getDeclaredFields();
		for (Field f : fields) {
			if (f.getName().toUpperCase().equals(variableName.toUpperCase())) {
				f.setAccessible(true);
				f.set(obj, value);
				f.setAccessible(false);
				break;
			}
		}
	}

}

package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;

import java.util.Map;

import org.exbeta.utilities.StringUtils;
import org.junit.Ignore;
import org.junit.Test;

public class StringUtilsTest {

	@Test
	@Ignore
	public void isNullTest() {
		assertEquals(StringUtils.isNull(""), true);
		assertEquals(StringUtils.isNull(null), true);
		assertEquals(StringUtils.isNull(" "), true);
	}

	@Test
	@Ignore
	public void parseToMapTest() {
		Map<String, String> map1 = StringUtils
				.parseToMapLikeWebSeparator("aaaa=dd;fsf=bb;");
		Map<String, String> map = StringUtils.parseToMap("aaaa=dd;fsf=bb;",
				";", "=");
		System.out.println(map.get("aaaa"));
		System.out.println(map.get("fsf"));
	}

	@Test
	@Ignore
	public void reverseTest() {
		String a = "abcdef";
		assertEquals(StringUtils.reverse(a), "fedcba");
	}

	@Test
	public void parseToObjectTest() throws InstantiationException, IllegalAccessException {
//		assertThat(StringUtils.parseToObject("i=100,j=200,v=-1.000000", ",",
//				"=", GribDomain.class), instanceOf(GribDomain.class));
//		GribDomain gribDomain = StringUtils.parseToObject("i=100,j=200,v=-1.000000", ",",
//				"=", GribDomain.class);
//		System.out.println("I value : " + gribDomain.getI());
//		System.out.println("J value : " + gribDomain.getJ());
//		System.out.println("V value : " + gribDomain.getV());
	}
}

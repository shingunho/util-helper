package test;

import org.exbeta.utilities.ObjectUtils;
import org.junit.Test;


public class ObjectUtilsTest {
	public static class sample {
		private String privateString = "strings";
		private Long privateLong = 10000L;
		private Integer privateInteger = 10;
		
		public String getPrivateString() {
			return privateString;
		}
		public void setPrivateString(String privateString) {
			this.privateString = privateString;
		}
		public Long getPrivateLong() {
			return privateLong;
		}
		public void setPrivateLong(Long privateLong) {
			this.privateLong = privateLong;
		}
		public Integer getPrivateInteger() {
			return privateInteger;
		}
		public void setPrivateInteger(Integer privateInteger) {
			this.privateInteger = privateInteger;
		}
	};
	@SuppressWarnings("unused")
	public static Object sample2 = new Object(){
		private String privateString = "strings";
		private Long privateLong = 10000L;
		private Integer privateInteger = 10;
		
		public String getPrivateString() {
			return privateString;
		}
		public void setPrivateString(String privateString) {
			this.privateString = privateString;
		}
		public Long getPrivateLong() {
			return privateLong;
		}
		public void setPrivateLong(Long privateLong) {
			this.privateLong = privateLong;
		}
		public Integer getPrivateInteger() {
			return privateInteger;
		}
		public void setPrivateInteger(Integer privateInteger) {
			this.privateInteger = privateInteger;
		}
	};
	
	@Test
	public void toStringTest() throws IllegalArgumentException, IllegalAccessException{
		System.out.println(ObjectUtils.toString(new ObjectUtilsTest.sample()));
		System.out.println(ObjectUtils.toString(sample2));
	}
}

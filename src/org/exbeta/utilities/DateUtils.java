package org.exbeta.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtils {

	private DateUtils(){}
	
	public static String getDateTime(String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(Calendar.getInstance().getTime());
	}

}
